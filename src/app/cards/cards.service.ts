import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';


@Injectable({
  providedIn: 'root'
})
export class CardsService {

  API_SERVER = "http://localhost:3000";
  constructor(private httpClient: HttpClient) {
      
   }
  
   public getCards(): Observable<any>{
       const endPoint = this.API_SERVER + '/creditcard'
    return this.httpClient.get(endPoint);  
}

  public createCard(data:any): Observable<any>{
    const endPoint = this.API_SERVER + '/creditcard'
    return this.httpClient.post(endPoint,data);
  }
}

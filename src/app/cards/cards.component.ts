import { Component, Input, OnInit } from '@angular/core';

import { CardsService } from './cards.service';

import * as bcrypt from 'bcryptjs';
const salt = bcrypt.genSaltSync(10);
@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.css']
})
export class CardsComponent implements OnInit {

  

   constructor(private cardsService: CardsService) {
    this.getCards();
   }

   async getCards(){
    this.cardsService.getCards().subscribe(data =>{
      console.log(data);
    });
   }

   
   async createCard(data:any){
    this.cardsService.createCard(data).subscribe(data =>{
      console.log(data); 
  });
  }

  cardnum : string ="";
  cardname : string ="";
  cardmouth : string="";
  cardyear : string="";
  cvv : string="";

  creditCardNumber: string="";

  ngOnInit(): void {
  
      
  }

  submit(){
    let data = {
      card_number: bcrypt.hashSync(this.cardnum, salt) ,
      card_name: bcrypt.hashSync(this.cardname, salt),
      expmm:bcrypt.hashSync(this.cardmouth, salt),
      expyy:bcrypt.hashSync(this.cardyear, salt),
      cvv: bcrypt.hashSync(this.cvv, salt),

    }
    this.createCard(data)
  }

  

}

